# -*- coding: utf-8 -*-
"""
@author: leogao
@file: process.py
@time: 2021/7/31 11:36 上午
"""

import json
import requests

# test_url = 'https://server-test.healbone.cn'
test_url = 'http://192.168.31.44:3000'
headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def cancel(token):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    url = '{}/patient/patient/'.format(test_url)
    data = {}
    res = requests.delete(url=url, data=data, headers=headers)


def send_mes(phone):
    """
    发送验证码
    :return:
    """
    url = '{}/patient/auth/HBsendSms'.format(test_url)
    data = {
        'phone': phone,
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def login(phone):
    """
    康复师登陆
    :return:
    """
    url = '{}/patient/patient/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def fill_in_information(token, name, type):
    """
    填写资料
    :return:
    """
    url = '{}/patient/patient'.format(test_url)
    if type == 'knee':
        data = {'user':
                    {'name': name,
                     'gender': 'M',
                     'birthday': '1971-01-01',
                     'height': 170,
                     'weight': 51,
                     'painParts': ['SHOULDER', 'BACK', 'HIP', 'KNEE'],
                     'treatingPart': 'KNEE'}}
    elif type == 'back':
        data = {'user':
                    {'name': name,
                     'gender': 'M',
                     'birthday': '1971-01-01',
                     'height': 170,
                     'weight': 51,
                     'painParts': ['SHOULDER', 'BACK', 'HIP', 'KNEE'],
                     'treatingPart': 'BACK'}}
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.put(headers=headers, url=url, data=json.dumps(data))
    print(res)
    print(res.json())


def get_process(token):
    """
    获取运动进程
    :return:
    """
    url = '{}/exercise/patient/levelProgress'.format(test_url)
    data = {
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.get(url=url, headers=headers, params=data)
    print(res)
    print(res.json())
    return res.json()['levelId']


def get_processtoday(token, level_name):
    """
    获取运动进程详情
    :return:
    """
    url = '{}/exercise/patient/levelToday'.format(test_url)
    data = {
        'levelId': level_name
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.get(url=url, headers=headers, params=data).json()
    return res


def yesterday_stats(token, data):
    """
    全局校准
    :return:
    """
    url = '{}/patient/patient/yesterdayStats'.format(test_url)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.put(url=url, headers=headers, data=json.dumps(data))
    print('yesterday_stats', res)
    print(res.json())


def level_status_calibration(token, levelSet_id, levelStats_id):
    """
    全局校准
    :return:
    """
    url = '{}/exercise/patient/levelStatsWholeCalibration'.format(test_url)
    data = {
        'levelStatsId': levelStats_id,
        'levelSetId': levelSet_id,
        'calibration':
            {'data':
                 [{'roll': -1.28,
                   'pitch': 11.290000000000006,
                   'yaw': 8.92,
                   'address': '34BD8ED0-FF8C-74B6-657B-F84DE78F8E82',
                   'deviceName': 'HEALBONE-241739',
                   'battery': [85]},
                  {'roll': 0,
                   'pitch': 0,
                   'yaw': 0,
                   'address': '',
                   'deviceName': '',
                   'battery': []}]},
        'sensorAnglesCal': '12345'
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.put(url=url, headers=headers, data=json.dumps(data))
    print(res.json())


def level_stats(*args, ):
    """
    每个运动的提交
    :return:
    """
    url = '{}/exercise/patient/levelStats'.format(test_url)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(args[0]),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'
    }
    data = args[1]
    res = requests.put(url=url, headers=headers, data=json.dumps(data))
    print(res.json())


def stats_whole(token_value, whole_data):
    """
    总的运动的提交
    :return:
    """
    url = '{}/exercise/patient/levelStatsWhole'.format(test_url)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token_value),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'
    }
    data = whole_data
    res = requests.put(url=url, headers=headers, data=json.dumps(data))
    print(res)
    print(res.json())


def reading(token_value, data_value):
    """
    阅读
    :return:
    """
    url = '{}/patient/patient/readingToday'.format(test_url)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token_value),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'
    }
    data = data_value
    res = requests.get(url=url, headers=headers, params=data)
    print(res)
    print(res.json())
    return res.json()


def reset_day(token_value, day):
    """
    重制患者状态
    :return:
    """
    url = '{}/exercise/patient/alterNewLevelCreateAt'.format(test_url)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token_value),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'
    }
    data = {
        # 'patientId': patient_id,
        'number': day
    }
    res = requests.post(url=url, headers=headers, data=json.dumps(data))
    print(res)
    print(res.json())


# first_score, second_score
def common_process(first_score, second_score, phone, type_):
    send_mes(phone)
    token_value = login(phone)
    fill_in_information(token_value, '测试', type_)
    # process接口获取到level_id
    level_id = get_process(token_value)
    # 获取到leveltoday的运动数据
    res_value = get_processtoday(token_value, level_id)
    levelSet_id = res_value['level']['levelSet']
    levelStats_id = res_value['level']['levelStats']
    # 填写昨日数据
    if type_ == 'knee':
        yesterday_data = {'kneePainScore': first_score, 'rigidnessScore': second_score}
        yesterday_stats(token_value, yesterday_data)
    elif type_ == 'back':
        yesterday_data = {'sleepScore': second_score, 'backPainScore': first_score}
        yesterday_stats(token_value, yesterday_data)

    # 全局校准
    level_status_calibration(token_value, levelSet_id, levelStats_id, )

    for key in res_value['level']['exercises']:
        dict_data = {'levelSetId': levelSet_id, 'levelStatsId': levelStats_id, 'exerciseId': key['exercise']['_id'],
                     'repeat': key['repeat'], 'holdtime': key['holdtime'], 'skippedAt': -1, 'useSensor': True,
                     'sensorAngles': '123', 'mId': key['mId'],
                     'duration': 75935, 'battery': [50], 'preparationDuration': 60}
        # 循环请求提交单个次数
        level_stats(token_value, dict_data)

    whole_one_data = {
        'fromPage': 'training',
        'levelStatsId': levelStats_id,
        'duration': 2469572,
        'score': 325
    }
    # 第一次请求whole，进入到患者评价
    stats_whole(token_value, whole_one_data)

    whole_two_data = {'levelStatsId': levelStats_id,
                      'feedback': 1,
                      'changeExercise': True}
    # 第二次请求whole，提交反馈
    stats_whole(token_value, whole_two_data)

    whole_three_data = {'levelStatsId': levelStats_id,
                        'exerciseSurvey': {'exercises': ['散步'], 'other': '哈哈哈', 'duration': '30'}
                        }

    # 第三次请求whole，提交运动收集
    stats_whole(token_value, whole_three_data)

    read_data_one = {
    }
    # read_id = reading(token_value, read_data_one)['reading']['id']
    #
    # read_data_two = {
    #     'duration': 251,
    #     'score': 5,
    #     'reading': read_id,
    #     'content': '哈哈哈'
    # }
    # reading(token_value, read_data_two)

    return token_value


# cancel(token_value)
def common_second_process(token_value, first_score, second_score, type_):
    level_id = get_process(token_value)
    # 获取到leveltoday的运动数据
    res_value = get_processtoday(token_value, level_id)
    levelSet_id = res_value['level']['levelSet']
    levelStats_id = res_value['level']['levelStats']
    # 填写昨日数据
    if type_ == 'knee':
        yesterday_data = {'kneePainScore': first_score, 'rigidnessScore': second_score}
        yesterday_stats(token_value, yesterday_data)
    elif type_ == 'back':
        yesterday_data = {'sleepScore': second_score, 'backPainScore': first_score}
        yesterday_stats(token_value, yesterday_data)


    # 全局校准
    level_status_calibration(token_value, levelSet_id, levelStats_id)

    for key in res_value['level']['exercises']:
        dict_data = {'levelSetId': levelSet_id, 'levelStatsId': levelStats_id, 'exerciseId': key['exercise']['_id'],
                     'repeat': key['repeat'], 'holdtime': key['holdtime'], 'skippedAt': -1, 'useSensor': True,
                     'sensorAngles': '123', 'mId': key['mId'],
                     'duration': 75935, 'battery': [50], 'preparationDuration': 60}
        # 循环请求提交单个次数
        level_stats(token_value, dict_data)

    whole_one_data = {
        'fromPage': 'training',
        'levelStatsId': levelStats_id,
        'duration': 2469572,
        'score': 325
    }
    # 第一次请求whole，进入到患者评价
    stats_whole(token_value, whole_one_data)

    whole_two_data = {'levelStatsId': levelStats_id,
                      'feedback': 1,
                      'changeExercise': True}
    # 第二次请求whole，提交反馈
    stats_whole(token_value, whole_two_data)
    # cancel(token_value)

def test1():
    token_value = common_process(60, 60, '94000000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 80, 60, 'back')


def test2():
    token_value = common_process(60, 60, '95000000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 60, 40, 'back')


def test3():
    token_value = common_process(60, 60, '96000000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 90, 60, 'back')


def test4():
    token_value = common_process(60, 90, '97000000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 60, 60, 'back')


def test5():
    token_value = common_process(60, 90, '98000000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 80, 49, 'back')


def test6():
    token_value = common_process(50, 90, '99000000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 85, 60, 'back')


def test7():
    token_value = common_process(71, 50, '99100000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 69, 60, 'back')


def test8():
    token_value = common_process(70, 50, '99200000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 70, 50, 'back')  # 不报错


def test9():
    token_value = common_process(70, 50, '99300000000', 'back')
    reset_day(token_value, 1)
    common_second_process(token_value, 71, 49, 'back') # 不报错


# test1()
def test10():
    token_value = common_process(60, 60, '81000000000', 'knee')
    reset_day(token_value, 1)
    common_second_process(token_value, 80, 60, 'knee')


def test11():
    token_value = common_process(60, 60, '82000000000', 'knee')
    reset_day(token_value, 1)
    common_second_process(token_value, 60, 80, 'knee')


def test12():
    token_value = common_process(30, 60, '83000000000', 'knee')
    reset_day(token_value, 1)
    common_second_process(token_value, 60, 60, 'knee')


def test13():
    token_value = common_process(60, 40, '84000000000', 'knee')
    reset_day(token_value, 1)
    common_second_process(token_value, 60, 70, 'knee')


def test14(): # 不报错
    token_value = common_process(40, 40, '85000000000', 'knee')
    reset_day(token_value, 1)
    common_second_process(token_value, 40, 40, 'knee')


def test15(): # 不报错
    token_value = common_process(70, 70, '86000000000', 'knee')
    reset_day(token_value, 1)
    common_second_process(token_value, 70, 70, 'knee')


# test1()

# level_id = get_process(token_value)
# # 获取到leveltoday的运动数据
# res_value = get_processtoday(token_value, level_id)
# yesterday_data_two = {'kneePainScore': 90, 'rigidnessScore': 46}
# yesterday_stats(token_value, yesterday_data_two)


# if __name__ == '__main__':
#     test1()
