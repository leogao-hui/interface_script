# -*- coding: utf-8 -*-
"""
@author: leogao
@file: theripast.py
@time: 2021/8/2 4:40 下午
"""
import requests
import json

# test_url = 'https://server-prod.healbone.cn'
test_url = 'https://server-test.healbone.cn'
headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def send_mes():
    """
    发送验证码
    :return:
    """
    url = '{}/therapist/auth/sendSms'.format(test_url)
    data = {
        'phone': '30000000000',
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def login():
    """
    康复师登陆
    :return:
    """
    url = '{}/therapist/therapist/tryLogin'.format(test_url)
    data = {
        'phone': '30000000000',
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def find_next_levelsets(token):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'}
    url = '{}/exercise/therapist/findNextLevelSets'.format(test_url)
    data = {
        'patientId': '6107a7079f743be8e4e28cbc'
    }
    res = requests.get(url=url, params=data, headers=headers)
    print(res.json())


def search_exercise(token):
    """
    查询运动
    :return:
    """
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    url = '{}/exercise/exercise/find'.format(test_url)
    data = {
        'category': '1',
        'sideSpec': '1',
        'difficulty': '1',
        'muscle': '1',
        'treatingPart': 'BACK'
    }
    print(url)
    res = requests.get(url=url, params=data, headers=headers)
    print(res.json())


def modify_levelset(token):
    url = '{}/exercise/therapist/alterLevelSetParams'.format(test_url)
    data = {'levelSetId': '6107a71e9f743be8e4e28cc2', 'currentLevel': 1, 'repeatDay': 5,
            'patientInfo': {'treatingPart': 'BACK', '_id': '6107a7079f743be8e4e28cbc', 'gender': '男', 'name': '测试接口',
                            'id': '6107a7079f743be8e4e28cbc', 'age': '42'},

            'exercises': [
                {'difficultyArr': [1, 2, 3, 4, 5, 6, 7], 'muscleArr': ['腹部肌群', '腰背肌群'], 'difficulty': 1, 'sideSpec': 1,
                 'muscle': [1], 'category': [1], 'treatingPart': 'BACK', 'name': '弯腰拾物',
                 'equipments': ['6101081ebcc82aced7dea7ae'], 'id': '6101165495a9a60a497146ba', 'holdtime': 3,
                 'repeat': 10,
                 'images': {'setup': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146ae',
                            'movement': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146af',
                            'setupMirror': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146b4',
                            'movementMirror': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146b5'}},
                {'difficultyArr': [1, 2, 3, 4, 5, 6, 7], 'muscleArr': ['腹部肌群', '腰背肌群'], 'difficulty': 1, 'sideSpec': 1,
                 'muscle': [1], 'category': [1], 'treatingPart': 'BACK', 'name': '弯腰拾物',
                 'equipments': ['6101081ebcc82aced7dea7b8'], 'id': '6101165495a9a60a497146ba', 'holdtime': 3,
                 'repeat': 10,
                 'images': {'setup': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146ae',
                            'movement': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146af',
                            'setupMirror': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146b4',
                            'movementMirror': 'https://server-prod.healbone.cn/common/file/6101165495a9a60a497146b5'}},
                {'difficultyArr': [1, 2, 3, 4, 5, 6, 7], 'muscleArr': ['腹部肌群', '腰背肌群'], 'difficulty': 1, 'sideSpec': 1,
                 'muscle': [1], 'category': [2], 'treatingPart': 'BACK', 'name': '坐位侧屈',
                 'equipments': ['6101081dbcc82aced7dea7a2'], 'id': '6101165395a9a60a49714671', 'holdtime': 30,
                 'repeat': 10,
                 'images': {'setup': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a49714665',
                            'movement': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a49714666',
                            'setupMirror': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a4971466b',
                            'movementMirror': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a4971466c'}},
                {'difficultyArr': [1, 2, 3, 4, 5, 6, 7], 'muscleArr': ['腹部肌群', '腰背肌群'], 'difficulty': 1, 'sideSpec': 2,
                 'muscle': [2], 'category': [1], 'treatingPart': 'BACK', 'name': '俯卧后伸',
                 'equipments': ['6101081dbcc82aced7dea7ac'], 'id': '6101165395a9a60a49714613', 'holdtime': 3,
                 'repeat': 10,
                 'images': {'setup': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a4971460d',
                            'movement': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a4971460e',
                            'movementMirror': 'https://server-prod.healbone.cn/common/file/undefined',
                            'setupMirror': 'https://server-prod.healbone.cn/common/file/undefined'}},
                {'difficultyArr': [1, 2, 3, 4, 5, 6, 7], 'muscleArr': ['腹部肌群', '腰背肌群'], 'difficulty': 1, 'sideSpec': 2,
                 'muscle': [2], 'category': [1], 'treatingPart': 'BACK', 'name': '俯卧后伸',
                 'equipments': ['6101081dbcc82aced7dea7ac'], 'id': '6101165395a9a60a49714613', 'holdtime': 3,
                 'repeat': 10,
                 'images': {'setup': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a4971460d',
                            'movement': 'https://server-prod.healbone.cn/common/file/6101165395a9a60a4971460e',
                            'movementMirror': 'https://server-prod.healbone.cn/common/file/undefined',
                            'setupMirror': 'https://server-prod.healbone.cn/common/file/undefined'}},
                {'difficultyArr': [1, 2, 3, 4, 5, 6, 7], 'muscleArr': ['腹部肌群', '腰背肌群'], 'difficulty': 1, 'sideSpec': 2,
                 'muscle': [2], 'category': [2], 'treatingPart': 'BACK', 'name': '站立腰部屈伸',
                 'equipments': ['6101081dbcc82aced7dea7a0'], 'id': '6101165595a9a60a4971471a', 'holdtime': 30,
                 'repeat': 10,
                 'images': {'setup': 'https://server-prod.healbone.cn/common/file/6101165595a9a60a49714714',
                            'movement': 'https://server-prod.healbone.cn/common/file/6101165595a9a60a49714715',
                            'movementMirror': 'https://server-prod.healbone.cn/common/file/undefined',
                            'setupMirror': 'https://server-prod.healbone.cn/common/file/undefined'}}

            ]}
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.put(url=url, data=json.dumps(data), headers=headers)
    print(res.json())


send_mes()
token_value = login()
# search_exercise(token_value)
modify_levelset(token_value)
# find_next_levelsets(token_value)

a = {'levelSetId': '610903565d2058a648b287f0', 'currentLevel': 1, 'repeatDay': 5,
     'patientInfo': {'treatingPart': 'BACK', '_id': '610903565d2058a648b287eb', 'gender': '男', 'name': '测试获取动作',
                     'id': '610903565d2058a648b287eb', 'age': '49'}, 'exercises': [({'difficultyArr': [1, 2, 3, 4, 5, 6,
                                                                                                       7],
                                                                                     'muscleArr': ['腹部肌群', '腰背肌群'],
                                                                                     'difficulty': 2, 'sideSpec': 2,
                                                                                     'muscle': [2], 'category': [2],
                                                                                     'treatingPart': 'BACK',
                                                                                     'name': '坐位腰背屈曲', 'equipments': [
            '6101081dbcc82aced7dea7a2'], 'id': '6101165595a9a60a497146f8', 'holdtime': 3, 'repeat': 5, 'images': {
            'setup': 'https://server-prod.healbone.cn/common/file/6108c24e5d2058a648b279a1',
            'movement': 'https://server-prod.healbone.cn/common/file/6108c24e5d2058a648b279a2',
            'setupMirror': 'https://server-prod.healbone.cn/common/file/null',
            'movementMirror': 'https://server-prod.healbone.cn/common/file/null'}},), {'difficultyArr': [1, 2, 3, 4, 5,
                                                                                                         6, 7],
                                                                                       'muscleArr': ['腹部肌群', '腰背肌群'],
                                                                                       'difficulty': 1, 'sideSpec': 1,
                                                                                       'muscle': [1], 'category': [1],
                                                                                       'treatingPart': 'BACK',
                                                                                       'name': '健身球站立对角线上举',
                                                                                       'equipments': [
                                                                                           '6101081ebcc82aced7dea7ae'],
                                                                                       'id': '6101165495a9a60a497146c7',
                                                                                       'holdtime': 3, 'repeat': 10,
                                                                                       'images': {
                                                                                           'setup': 'https://server-prod.healbone.cn/common/file/6108c2525d2058a648b27ab5',
                                                                                           'movement': 'https://server-prod.healbone.cn/common/file/6108c2525d2058a648b27ab6',
                                                                                           'setupMirror': 'https://server-prod.healbone.cn/common/file/6108c2525d2058a648b27abb',
                                                                                           'movementMirror': 'https://server-prod.healbone.cn/common/file/6108c2525d2058a648b27abc'}},
                                                                                   {'difficultyArr': [1, 2, 3, 4, 5, 6,
                                                                                                      7],
                                                                                    'muscleArr': ['腹部肌群', '腰背肌群'],
                                                                                    'difficulty': 1, 'sideSpec': 1,
                                                                                    'muscle': [1], 'category': [2],
                                                                                    'treatingPart': 'BACK',
                                                                                    'name': '坐姿腰方肌伸展', 'equipments': [
                                                                                       '6101081dbcc82aced7dea7ac'],
                                                                                    'id': '6101165395a9a60a49714649',
                                                                                    'holdtime': 30, 'repeat': 10,
                                                                                    'images': {
                                                                                        'setup': 'https://server-prod.healbone.cn/common/file/6108c24e5d2058a648b279ad',
                                                                                        'movement': 'https://server-prod.healbone.cn/common/file/6108c24e5d2058a648b279ae',
                                                                                        'setupMirror': 'https://server-prod.healbone.cn/common/file/6108c24e5d2058a648b279b3',
                                                                                        'movementMirror': 'https://server-prod.healbone.cn/common/file/6108c24e5d2058a648b279b4'}},
                                                                                   {'difficultyArr': [1, 2, 3, 4, 5, 6,
                                                                                                      7],
                                                                                    'muscleArr': ['腹部肌群', '腰背肌群'],
                                                                                    'sideSpec': 1, 'difficulty': 1,
                                                                                    'muscle': [2], 'category': [1]}, {
                                                                                       'difficultyArr': [1, 2, 3, 4, 5,
                                                                                                         6, 7],
                                                                                       'muscleArr': ['腹部肌群', '腰背肌群'],
                                                                                       'difficulty': 1, 'sideSpec': 2,
                                                                                       'muscle': [2], 'category': [1],
                                                                                       'treatingPart': 'BACK',
                                                                                       'name': '俯卧后伸', 'equipments': [
                '6101081dbcc82aced7dea7ac'], 'id': '6101165395a9a60a49714613', 'holdtime': 3, 'repeat': 10, 'images': {
                'setup': 'https://server-prod.healbone.cn/common/file/6108c2555d2058a648b27b33',
                'movement': 'https://server-prod.healbone.cn/common/file/6108c2555d2058a648b27b34',
                'setupMirror': 'https://server-prod.healbone.cn/common/file/null',
                'movementMirror': 'https://server-prod.healbone.cn/common/file/null'}}, {
                                                                                       'difficultyArr': [1, 2, 3, 4, 5,
                                                                                                         6, 7],
                                                                                       'muscleArr': ['腹部肌群', '腰背肌群'],
                                                                                       'difficulty': 1, 'sideSpec': 2,
                                                                                       'muscle': [2], 'category': [2],
                                                                                       'treatingPart': 'BACK',
                                                                                       'name': '坐姿髋关节屈曲',
                                                                                       'equipments': [
                                                                                           '6101081dbcc82aced7dea7a2'],
                                                                                       'id': '6101165395a9a60a4971463c',
                                                                                       'holdtime': 30, 'repeat': 10,
                                                                                       'images': {
                                                                                           'setup': 'https://server-prod.healbone.cn/common/file/6108c24d5d2058a648b27983',
                                                                                           'movement': 'https://server-prod.healbone.cn/common/file/6108c24d5d2058a648b27984',
                                                                                           'setupMirror': 'https://server-prod.healbone.cn/common/file/null',
                                                                                           'movementMirror': 'https://server-prod.healbone.cn/common/file/null'}}]}
