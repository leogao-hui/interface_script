# -*- coding: utf-8 -*-
"""
@author: leogao
@file: test_json.py
@time: 2021/8/24 4:47 下午
"""
import json

a = {'patientAlerts': [{'scoreList': {'sleeps': {'dateStr': '2021-08-25', 'date': '2021-08-26T08:11:00.301Z', 'score': 48, 'difference': -22}, 'rigidness': {}, 'kneePains': {}, 'backPains': {'dateStr': '2021-08-25', 'date': '2021-08-26T08:11:00.301Z', 'score': 80, 'difference': 20}}, 'status': 1, '_id': '61274c95e93766627249669a', 'stuckPreparation': [], 'skipPreparation': [], 'skipMovement': [], 'errorPreparation': [], 'errorMovement': [], 'treatingPart': 'BACK', 'patient': '61274c91e93766627249666b', 'phone': '66200000000', 'name': '测试', 'levelStats': '61274c93e93766627249668e', 'repeat': None, 'schedule': None, 'feedback': None, 'createdAt': '2021-08-26T08:11:01.116Z', 'updatedAt': '2021-08-26T08:11:01.116Z', '__v': 0, 'id': '61274c95e93766627249669a'}], 'allPage': 1, 'curPage': 1}

with open('test.json', 'w', encoding='utf-8') as f:
    f.write(json.dumps(a))