# -*- coding: utf-8 -*-
"""
@author: leogao
@file: update.py
@time: 2021/8/13 4:13 下午
"""
import json

import requests

test_url = 'https://server-test.healbone.cn'
headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def send_mes():
    """
    发送验证码
    :return:
    """
    url = '{}/patient/auth/sendSms'.format(test_url)
    data = {
        'phone': '62000000000',
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def login():
    """
    康复师登陆
    :return:
    """
    url = '{}/patient/patient/tryLogin'.format(test_url)
    data = {
        'phone': '62000000000',
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def update_version(token):
    url = '{}/common/appUpdateVersion/latestVersion'.format(test_url)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "patient"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 105, 80, 104, 111, 110, 101, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 105, 80, 104, 111, 110, 101, 32, 88, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 105, 79, 83, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 34, 49, 52, 46, 51, 34, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 69, 50, 66, 55, 54, 50, 67, 49, 45, 68, 53, 50, 49, 45, 52, 55, 57, 56, 45, 65, 66, 57, 48, 45, 70, 66, 53, 49, 54, 53, 53, 57, 48, 67, 51, 70, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 65, 112, 112, 108, 101, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'
    }
    data = {
    }
    res = requests.get(headers=headers, url=url, data=data)
    print(res)
    print(res.json())

