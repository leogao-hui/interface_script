# -*- coding: utf-8 -*-
"""
@author: leogao
@file: modify_exercises.py
@time: 2021/8/26 7:36 下午
"""
import requests
import json

test_url = 'https://server-test.healbone.cn'

phone = '98534000000'
th_phone = '20000000000'
type_ = 'BACK'


def read_data(txt):

    list_data = []
    with open(txt, 'r', encoding='utf-8') as f:
        while True:
            data = f.readline().strip()
            if data:
                list_data.append(data)
            if not data:
                break
    return list_data

# read_data('data.txt')


headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def send_mes():
    """
    发送验证码
    :return:
    """
    url = '{}/patient/auth/sendSms'.format(test_url)
    data = {
        'phone': phone,
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }

    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def login():
    """
    康复师登陆
    :return:
    """
    url = '{}/patient/patient/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    print(res.json())
    value = res.json()
    print(value)
    return value


def fill_in_information(token):
    """
    填写资料
    :return:
    """
    url = '{}/patient/patient'.format(test_url)
    print('url', url)
    data = {'user':
                {'name': '测试',
                 'gender': 'M',
                 'birthday': '1971-01-01',
                 'height': 170,
                 'weight': 51,
                 'painParts': ['SHOULDER', 'BACK', 'HIP', 'KNEE'],
                 'treatingPart': 'BACK'}}
    headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(headers=headers, url=url, data=json.dumps(data))
    print(res)
    print(res.json())


def get_process(token):
    """
    获取运动进程
    :return:
    """
    url = '{}/exercise/patient/levelProgress'.format(test_url)
    data = {
    }
    headers['Authorization'] = 'Token {}'.format(token)
    res = requests.get(url=url, headers=headers, params=data)
    print(res)
    print(res.json()['levelId'])
    return res.status_code, res.json()['levelId']


def get_details_exercise(level_id, token):
    """
    获取详细运动信息
    :return:
    """
    headers['Authorization'] = 'Token {}'.format(token)
    url = '{}/exercise/patient/levelToday'.format(test_url)
    data = {
        'levelId': level_id
    }
    res = requests.get(url=url, params=data, headers=headers)
    print(res.status_code)
    return res.status_code


def therapist_send_mes():
    """
    发送验证码
    :return:
    """
    url = '{}/therapist/auth/sendSms'.format(test_url)
    data = {
        'phone': th_phone,
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def therapist_login():
    """
    康复师登陆
    :return:
    """
    url = '{}/therapist/therapist/tryLogin'.format(test_url)
    data = {
        'phone': th_phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def find_next_levelsets(token, patientId):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'}
    url = '{}/exercise/therapist/findNextLevelSets'.format(test_url)
    data = {
        'patientId': patientId
    }
    res = requests.get(url=url, params=data, headers=headers)
    return res.json()


def add_phone_whiteList(token, phone) -> dict:
    """
    康复师添加白名单
    :param phone: 病人手机号
    :return:
    """
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'}

    data = {
        'phone': phone
    }
    url = '{}/common/phoneWhitelist'.format(test_url)
    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    return res.json()


def alter_level_set(token, data: dict) -> dict:
    """
    修改动作
    :param data: 接口传参
    :return:
    """
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'}

    res = requests.put(headers=headers, url='{}/exercise/therapist/alterLevelSetParams'.format(test_url), data=json.dumps(data))
    return res.json()


def search_ids(token, eng_name):
    """
    根据动作名称和治疗部位获取 ID
    :param eng_name:
    :param token:
    :return:
    """
    data = {
        'treatingPart': type_,
        'nameMedbridge': eng_name
    }
    headers['Authorization'] = 'Token {}'.format(token)
    url = '{}/exercise/exercise/ids'.format(test_url)
    res =requests.get(url=url, headers=headers, params=data)
    return res.json()['ids'][0]


def search_two(id_list):

    url = '{}/exercise/exercise/exerciseFindTest2'.format(test_url)
    data = {
        'exercises': id_list
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    return res.json()['results']


def search_one():

    url = '{}/exercise/exercise/exerciseFindTest'.format(test_url)
    data = {
        'treatingPart': type_
    }
    res = requests.get(url=url, params=data, headers=headers)
    print('search_one', res.json())
    return res.json()['results']


therapist_send_mes()
th_token = therapist_login()
add_phone_whiteList(th_token, phone)
send_mes()
res_login = login()
pt_token = res_login['user']['token']
pt_id = res_login['user']['id']
fill_in_information(pt_token)
get_process(pt_token)
res_find_value = find_next_levelsets(th_token, pt_id)
print('res_find_value', res_find_value)
# # 第一种
res = search_one()

# 第二种
# name_list = read_data('data.txt')
# id_list_data = []
# for key in name_list:
#     id_list_data.append(search_ids(pt_token, key))
# res = search_two(id_list_data)


res_find_value['exercises'].clear()
for i in res:
    res_find_value['exercises'].append(i)

alter_level_set(th_token, res_find_value)





