# -*- coding: utf-8 -*-
"""
@author: leogao
@file: register.py
@time: 2021/8/1 6:01 下午
"""
import json

import requests

list_data = {
    '詹志丽' :'13813985214',
    '董闫妍' : '13910885845',
    '王苏阳' : '13651238510',
    '周维敏' : '17702506268',
    '江维佳' :'13913915651',
    '赵文蓉' : '13593145295',
    '唐美如' : '13376097671',
    '周茜' :'13770709837',
    '鞠英' :'13501212802',
    '卲泽江': '18936638006',
    '王燕华':'13501182140',
    '高永信': '13911278719',
    '罗辑': '13001930707',
    '马学海':'13901310665',
    '苏景艳':'15811372756',
    '雷春蓉': '18114705700',
    '王军':'13521791519',
    '陈洁敏': '13520900100'

}

# test_url = 'https://server-prod.healbone.cn'
test_url = 'https://server-test.healbone.cn'
headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def send_mes(phone):
    """
    发送验证码
    :return:
    """
    url = '{}/patient/auth/HBsendSms'.format(test_url)
    data = {
        'phone': phone,
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def login(phone):
    """
    康复师登陆
    :return:
    """
    url = '{}/patient/patient/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def fill_in_information(token ,name):
    """
    填写资料
    :return:
    """
    url = '{}/patient/patient'.format(test_url)
    print('url', url)
    data = {'user':
                {'name': name,
                 'gender': 'M',
                 'birthday': '1971-01-01',
                 'height': 170,
                 'weight': 51,
                 'painParts': ['SHOULDER', 'BACK', 'HIP', 'KNEE'],
                 'treatingPart': 'BACK'}}
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    res = requests.put(headers=headers, url=url, data=json.dumps(data))
    print(res)
    print(res.json())


# for key, value in enumerate(list_data):
#     print(list_data[value], value)
#
#     send_mes(list_data[value])
#     token_value = login(list_data[value])
#     fill_in_information(token_value, value)
#     #get_process(token_value)


send_mes('93000000000')
token_value = login('93000000000')
fill_in_information(token_value, '测试')