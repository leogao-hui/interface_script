# -*- coding: utf-8 -*-
"""
@author: leogao
@file: many_day_no_exercise.py
@time: 2021/8/24 3:33 下午
"""
import requests
import json

# test_url = 'https://server-dev.healbone.cn'
# test_url = 'https://server-test.healbone.cn'
test_url = 'http://192.168.31.44:3000'
# test_url = 'https://server-prod.healbone.cn'

web_headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}

app_headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "patient"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def cancel(token):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "patient"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    url = '{}/patient/patient/'.format(test_url)
    data = {}
    res = requests.delete(url=url, data=data, headers=headers)


def patient_send_mes(phone):
    """
    发送验证码
    :return:
    """
    url = '{}/patient/auth/sendSms'.format(test_url)
    data = {
        'phone': phone,
        'encryption': 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=app_headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def patient_login(phone):
    """
    病人登录
    :return:
    """
    url = '{}/patient/patient/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=app_headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def add_whitelist(token, phone):
    """
    康复师端添加白名单
    :return:
    """
    url = '{}/common/phoneWhitelist/'.format(test_url)
    web_headers['Authorization'] = 'Token {}'.format(token)
    data = {
        "phone": phone
    }
    res = requests.post(url=url, headers=web_headers, data=json.dumps(data))
    print('添加白名单', res)
    print(res.json())


def therapist_send_mes(phone):
    """
    发送验证码
    :return:
    """
    url = '{}/therapist/auth/sendSms'.format(test_url)
    data = {
        'phone': phone,
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=web_headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def therapist_login(phone):
    """
    康复师登陆
    :return:
    """
    url = '{}/therapist/therapist/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=web_headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def search_warning(token, data):
    """
    全局校准
    :return:
    """
    web_headers['Authorization'] = 'Token {}'.format(token)
    print(web_headers)
    url = '{}/therapist/warning/findWarning'.format(test_url)
    data = {
        'phoneQuery': data,
        "pageIndex": 1,
        "treatingPart": ['KNEE', 'BACK'],
        "status": [1, 2],
        "startTime": "2021-8-15",
        "endTime": "2021-8-21",
    }
    res = requests.post(url=url, headers=web_headers, data=json.dumps(data))
    print(res)
    print('查询警告', res.json())
    return res.json()


def fill_in_information(token, name):
    """
    填写资料
    :return:
    """
    url = '{}/patient/patient'.format(test_url)
    data = {'user':
                {'name': name,
                 'gender': 'M',
                 'birthday': '1971-01-01',
                 'height': 170,
                 'weight': 51,
                 'painParts': ['SHOULDER', 'BACK', 'HIP', 'KNEE'],
                 'treatingPart': 'BACK'}}
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(headers=app_headers, url=url, data=json.dumps(data))
    print(res)
    print(res.json())


def get_process(token):
    """
    获取运动进程
    :return:
    """
    url = '{}/exercise/patient/levelProgress'.format(test_url)
    data = {
    }
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.get(url=url, headers=app_headers, params=data)
    print(res)
    print(res.json())
    return res.json()['levelId']


def get_processtoday(token, level_name):
    """
    获取运动进程详情
    :return:
    """
    url = '{}/exercise/patient/levelToday'.format(test_url)
    data = {
        'levelId': level_name
    }
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.get(url=url, headers=app_headers, params=data).json()
    return res


def yesterday_stats(token, data):
    """
    全局校准
    :return:
    """
    url = '{}/patient/patient/yesterdayStats'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res)
    print(res.json())


def level_status_calibration(token, levelSet_id, levelStats_id):
    """
    全局校准
    :return:
    """
    url = '{}/exercise/patient/levelStatsWholeCalibration'.format(test_url)
    data = {
        'levelStatsId': levelStats_id,
        'levelSetId': levelSet_id,
        'calibration':
            {'data':
                 [{'roll': -1.28,
                   'pitch': 11.290000000000006,
                   'yaw': 8.92,
                   'address': '34BD8ED0-FF8C-74B6-657B-F84DE78F8E82',
                   'deviceName': 'HEALBONE-241739',
                   'battery': [85]},
                  {'roll': 0,
                   'pitch': 0,
                   'yaw': 0,
                   'address': '',
                   'deviceName': '',
                   'battery': []}]},
        'sensorAnglesCal': '12345'
    }
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res.json())


def level_stats(*args, ):
    """
    每个运动的提交
    :return:
    """
    url = '{}/exercise/patient/levelStats'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(args[0])
    data = args[1]
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res.json())


def error_report(*args):
    """
    报错
    :return:
    """
    url = '{}/exercise/patient/exerciseNewErrorReport'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(args[0])
    res = requests.post(url=url, headers=app_headers, data=json.dumps(args[1]))
    print(res)
    print(res.json())


def stats_whole(token_value, whole_data):
    """
    总的运动的提交
    :return:
    """
    url = '{}/exercise/patient/levelStatsWhole'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token_value)
    data = whole_data
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res)
    print(res.json())


def reading(token_value, data_value):
    """
    阅读
    :return:
    """
    url = '{}/patient/patient/readingToday'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token_value)
    data = data_value
    res = requests.get(url=url, headers=app_headers, params=data)
    print(res)
    print(res.json())
    return res.json()


def reset_day(token_value, day):
    """
    重制患者状态
    :return:
    """
    url = '{}/exercise/patient/alterNewLevelCreateAt'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token_value)
    data = {
        # 'patientId': patient_id,
        'n': day
    }
    res = requests.post(url=url, headers=app_headers, data=json.dumps(data))
    print(res)
    print(res.json())


def find_exercise_patient(token_value, phone):
    """
    查询未训练患者
    :param token_value:
    :return:
    """
    web_headers['Authorization'] = 'Token {}'.format(token_value)
    url = '{}/therapist/warning/findWarningPatient'.format(test_url)
    data = {
        'queryString': phone,
        "pageIndex": 1,
        "treatingPart": ['KNEE', 'BACK'],
        "trackedStatus": 1,
    }
    res = requests.post(headers=web_headers, url=url, data=json.dumps(data))
    print(res)
    print('查询未训练患者', res.json())
    return res.json()


# phone = '91000000000'


# first_score, second_score
def common_process(therapist_phone, patient_phone, skippedAt):
    therapist_send_mes(therapist_phone)
    therapist_token_value = therapist_login(therapist_phone)
    add_whitelist(therapist_token_value, patient_phone)
    patient_send_mes(patient_phone)
    token_value = patient_login(patient_phone)
    fill_in_information(token_value, '测试')
    # process接口获取到level_id
    level_id = get_process(token_value)
    # 获取到leveltoday的运动数据
    res_value = get_processtoday(token_value, level_id)
    levelSet_id = res_value['level']['levelSet']
    levelStats_id = res_value['level']['levelStats']
    # 填写昨日数据
    yesterday_data = {'sleepScore': 50, 'backPainScore': 70}
    yesterday_stats(token_value, yesterday_data)

    # 全局校准
    level_status_calibration(token_value, levelSet_id, levelStats_id, )

    for key in res_value['level']['exercises']:
        dict_data = {'levelSetId': levelSet_id, 'levelStatsId': levelStats_id, 'exerciseId': key['exercise']['_id'],
                     'repeat': key['repeat'], 'holdtime': key['holdtime'], 'skippedAt': skippedAt, 'useSensor': True,
                     'sensorAngles': '123', 'mId': key['mId'],
                     'duration': 75935, 'battery': [50], 'preparationDuration': 30}  # 卡准备页时间
        # 循环请求提交单个次数
        level_stats(token_value, dict_data)

    whole_one_data = {
        'fromPage': 'training',
        'levelStatsId': levelStats_id,
        'duration': 2469572,
        'score': 325
    }
    # 第一次请求whole，进入到患者评价
    stats_whole(token_value, whole_one_data)

    whole_two_data = {'levelStatsId': levelStats_id,
                      'feedback': 3,
                      'changeExercise': True}
    # 第二次请求whole，提交反馈
    stats_whole(token_value, whole_two_data)
    whole_three_data = {'levelStatsId': levelStats_id,
                        'exerciseSurvey': {'exercises': ['散步'], 'other': '哈哈哈', 'duration': '30'}
                        }

    # 第三次请求whole，提交运动收集
    stats_whole(token_value, whole_three_data)

    read_data_one = {
    }
    read_id = reading(token_value, read_data_one)['reading']['id']

    read_data_two = {
        'duration': 251,
        'score': 5,
        'reading': read_id,
        'content': '哈哈哈'
    }
    reading(token_value, read_data_two)

    return therapist_token_value, token_value


# 当患者注册后，没有进行任何操作，注册完成后24小时后在表中不会查询到该患者
def test1():
    therapist_send_mes('30000000000')
    therapist_token_value = therapist_login('30000000000')
    add_whitelist(therapist_token_value, '90000000000')
    patient_send_mes('90000000000')
    token_value = patient_login('90000000000')
    fill_in_information(token_value, '测试')
    reset_day(token_value, 1)
    res = find_exercise_patient(therapist_token_value, '90000000000')
    assert [] == res['patients']


# 患者注册后，进行了一系列操作，但是没有提交完运动，注册成功的72小时后未完成运动，在表中会查询到该患者，未训练天数为4天
def test2():
    therapist_send_mes('30000000000')
    therapist_token_value = therapist_login('30000000000')
    add_whitelist(therapist_token_value, '98000000000')
    patient_send_mes('98000000000')
    token_value = patient_login('98000000000')
    fill_in_information(token_value, '测试')

    level_id = get_process(token_value)
    # 获取到leveltoday的运动数据
    res_value = get_processtoday(token_value, level_id)
    levelSet_id = res_value['level']['levelSet']
    levelStats_id = res_value['level']['levelStats']
    # 填写昨日数据
    yesterday_data = {'sleepScore': 50, 'backPainScore': 70}
    yesterday_stats(token_value, yesterday_data)

    # 全局校准
    level_status_calibration(token_value, levelSet_id, levelStats_id, )

    for key in res_value['level']['exercises']:
        dict_data = {'levelSetId': levelSet_id, 'levelStatsId': levelStats_id, 'exerciseId': key['exercise']['_id'],
                     'repeat': key['repeat'], 'holdtime': key['holdtime'], 'skippedAt': 0, 'useSensor': True,
                     'sensorAngles': '123', 'mId': key['mId'],
                     'duration': 75935, 'battery': [50], 'preparationDuration': 30}  # 卡准备页时间
        # 循环请求提交单个次数
        level_stats(token_value, dict_data)
    reset_day(token_value, 3)

    find_exercise_patient(therapist_token_value, '98000000000')


# 当患者注册后，没有进行任何操作，注册完成后72小时后在表中会查询到该患者，未训练天数为4天
def test3():
    therapist_send_mes('30000000000')
    therapist_token_value = therapist_login('30000000000')
    add_whitelist(therapist_token_value, '99100000000')
    patient_send_mes('99100000000')
    token_value = patient_login('99100000000')
    fill_in_information(token_value, '测试')
    level_id = get_process(token_value)
    reset_day(token_value, 3)
    find_exercise_patient(therapist_token_value, '99100000000')


# 患者注册后，提交完运动，提交完成功的72小时后未完成运动，在表中会查询到该患者，未训练天数为4天
def test4():
    res = common_process('30000000000', '93000000000', 0)
    therapist_token_value = res[0]
    token_value = res[1]
    reset_day(token_value, 3)
    res = find_exercise_patient(therapist_token_value, '93000000000')
    assert res['patients']['days'] == 4


# 患者注册后，提交完运动，提交完成功的96小时后未完成运动，在表中会查询到该患者，未训练天数为5天
def test5():
    res = common_process('30000000000', '94000000000', 0)
    therapist_token_value = res[0]
    token_value = res[1]
    reset_day(token_value, 4)
    res = find_exercise_patient(therapist_token_value, '94000000000')
    assert res['patients'][0]['days'] == '5'


# 当患者注册后，做一套动作的时候跳过了所有动作，提交完成功的72小时内未完成运动，在表中展示的训练完成为0
def test6():
    res = common_process('30000000000', '95400000000', 0)
    therapist_token_value = res[0]
    token_value = res[1]
    reset_day(token_value, 3)
    res = find_exercise_patient(therapist_token_value, '95100000000')
    assert res['patients'][0]['lastSchedule'] == 0


# 当患者注册后，做一套动作的时候没有跳过任何动作，提交完成功的72小时内未完成运动，在表中展示的训练完成为100
def test7():
    res = common_process('30000000000', '96000000000', -1)
    therapist_token_value = res[0]
    token_value = res[1]
    reset_day(token_value, 4)
    res = find_exercise_patient(therapist_token_value, '96000000000')
    assert res['patients'][0]['lastSchedule'] == 1
