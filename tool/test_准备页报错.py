# -*- coding: utf-8 -*-
"""
@author: leogao
@file: evaluate_difficult.py
@time: 2021/8/19 10:41 上午
"""
import json
import requests

test_url = 'https://server-dev.healbone.cn'
# test_url = 'http://192.168.31.44:3000'
# test_url = 'https://server-prod.healbone.cn'

web_headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "therapistWeb"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}

app_headers = {
    'Content-Type': 'application/json',
    'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "patient"}',
    'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

}


def cancel(token):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token {}'.format(token),
        'appInfo': '{"version":"2.0.0","buildNumber":"1", "appName": "patient"}',
        'deviceInfo': '[123, 34, 109, 111, 100, 101, 108, 34, 58, 34, 79, 83, 49, 48, 53, 34, 44, 34, 110, 97, 109, 101, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 32, 79, 83, 49, 48, 53, 34, 44, 34, 115, 121, 115, 116, 101, 109, 78, 97, 109, 101, 34, 58, 34, 65, 110, 100, 114, 111, 105, 100, 34, 44, 34, 115, 121, 115, 116, 101, 109, 86, 101, 114, 115, 105, 111, 110, 34, 58, 50, 53, 44, 34, 105, 115, 80, 104, 121, 115, 105, 99, 97, 108, 68, 101, 118, 105, 99, 101, 34, 58, 116, 114, 117, 101, 44, 34, 105, 100, 101, 110, 116, 105, 102, 105, 101, 114, 34, 58, 34, 56, 56, 51, 100, 97, 99, 53, 97, 102, 52, 55, 49, 51, 98, 50, 53, 34, 44, 34, 109, 97, 110, 117, 102, 97, 99, 116, 117, 114, 101, 114, 34, 58, 34, 115, 109, 97, 114, 116, 105, 115, 97, 110, 34, 44, 34, 112, 108, 97, 116, 102, 111, 114, 109, 34, 58, 34, 77, 111, 98, 105, 108, 101, 34, 125]'

    }
    url = '{}/patient/patient/'.format(test_url)
    data = {}
    res = requests.delete(url=url, data=data, headers=headers)


def patient_send_mes(phone):
    """
    发送验证码
    :return:
    """
    url = '{}/patient/auth/sendSms'.format(test_url)
    data = {
        'phone': phone,
        'encryption': 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=app_headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def patient_login(phone):
    """
    病人登录
    :return:
    """
    url = '{}/patient/patient/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=app_headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def add_whitelist(token, phone):
    """
    康复师端添加白名单
    :return:
    """
    url = '{}/common/phoneWhitelist/'.format(test_url)
    web_headers['Authorization'] = 'Token {}'.format(token)
    data = {
        "phone": phone
    }
    res = requests.post(url=url, headers=web_headers, data=json.dumps(data))
    print('添加白名单', res)
    print(res.json())


def therapist_send_mes(phone):
    """
    发送验证码
    :return:
    """
    url = '{}/therapist/auth/sendSms'.format(test_url)
    data = {
        'phone': phone,
        "encryption": 'b7WPvUBviN + tp0c / pXf1jb2qsJ + RYz6kSanIXrOavaiBrJGBwszqlTO9XPgWuCezDzjA / d58n1mHvTFR5zdkrKt4xp3CqhDXQv06icIcw21ALd2pSluQn0LRYCzdeH + bs4Mhz2wJdxHpt3XRmwbXKZXwcVe / jezIbbThV3Vscy0 ='
    }
    res = requests.post(headers=web_headers, url=url, data=json.dumps(data))
    print('发送验证码', res.json())


def therapist_login(phone):
    """
    康复师登陆
    :return:
    """
    url = '{}/therapist/therapist/tryLogin'.format(test_url)
    data = {
        'phone': phone,
        'smsToken': '66666'
    }
    res = requests.post(url=url, data=json.dumps(data), headers=web_headers)
    print(res.json())
    value = res.json()['user']['token']
    print(value)
    return value


def search_warning(token, data):
    """
    全局校准
    :return:
    """
    web_headers['Authorization'] = 'Token {}'.format(token)
    print(web_headers)
    url = '{}/therapist/warning/findWarning'.format(test_url)
    data = {
        'phoneQuery': data,
        "pageIndex": 1,
        "treatingPart": ['KNEE', 'BACK'],
        "status": [1, 2],
        "startTime": "2021-8-15",
        "endTime": "2021-8-21",
    }
    res = requests.post(url=url, headers=web_headers, data=json.dumps(data))
    print(res)
    print('查询警告', res.json())
    return res.json()


def fill_in_information(token, name):
    """
    填写资料
    :return:
    """
    url = '{}/patient/patient'.format(test_url)
    data = {'user':
                {'name': name,
                 'gender': 'M',
                 'birthday': '1971-01-01',
                 'height': 170,
                 'weight': 51,
                 'painParts': ['SHOULDER', 'BACK', 'HIP', 'KNEE'],
                 'treatingPart': 'BACK'}}
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(headers=app_headers, url=url, data=json.dumps(data))
    print(res)
    print(res.json())


def get_process(token):
    """
    获取运动进程
    :return:
    """
    url = '{}/exercise/patient/levelProgress'.format(test_url)
    data = {
    }
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.get(url=url, headers=app_headers, params=data)
    print(res)
    print(res.json())
    return res.json()['levelId']


def get_processtoday(token, level_name):
    """
    获取运动进程详情
    :return:
    """
    url = '{}/exercise/patient/levelToday'.format(test_url)
    data = {
        'levelId': level_name
    }
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.get(url=url, headers=app_headers, params=data).json()
    return res


def yesterday_stats(token, data):
    """
    全局校准
    :return:
    """
    url = '{}/patient/patient/yesterdayStats'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res)
    print(res.json())


def level_status_calibration(token, levelSet_id, levelStats_id):
    """
    全局校准
    :return:
    """
    url = '{}/exercise/patient/levelStatsWholeCalibration'.format(test_url)
    data = {
        'levelStatsId': levelStats_id,
        'levelSetId': levelSet_id,
        'calibration':
            {'data':
                 [{'roll': -1.28,
                   'pitch': 11.290000000000006,
                   'yaw': 8.92,
                   'address': '34BD8ED0-FF8C-74B6-657B-F84DE78F8E82',
                   'deviceName': 'HEALBONE-241739',
                   'battery': [85]},
                  {'roll': 0,
                   'pitch': 0,
                   'yaw': 0,
                   'address': '',
                   'deviceName': '',
                   'battery': []}]},
        'sensorAnglesCal': '12345'
    }
    app_headers['Authorization'] = 'Token {}'.format(token)
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res.json())


def level_stats(*args, ):
    """
    每个运动的提交
    :return:
    """
    url = '{}/exercise/patient/levelStats'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(args[0])
    data = args[1]
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res.json())


def error_report(*args):
    """
    报错
    :return:
    """
    url = '{}/exercise/patient/exerciseNewErrorReport'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(args[0])
    res = requests.post(url=url, headers=app_headers, data=json.dumps(args[1]))
    print(res)
    print(res.json())


def stats_whole(token_value, whole_data):
    """
    总的运动的提交
    :return:
    """
    url = '{}/exercise/patient/levelStatsWhole'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token_value)
    data = whole_data
    res = requests.put(url=url, headers=app_headers, data=json.dumps(data))
    print(res)
    print(res.json())


def reading(token_value, data_value):
    """
    阅读
    :return:
    """
    url = '{}/patient/patient/readingToday'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token_value)
    data = data_value
    res = requests.get(url=url, headers=app_headers, params=data)
    print(res)
    print(res.json())
    return res.json()


def reset_day(token_value, day):
    """
    重制患者状态
    :return:
    """
    url = '{}/exercise/patient/alterNewLevelCreateAt'.format(test_url)
    app_headers['Authorization'] = 'Token {}'.format(token_value)
    data = {
        # 'patientId': patient_id,
        'number': day
    }
    res = requests.post(url=url, headers=app_headers, data=json.dumps(data))
    print(res)
    print(res.json())

# phone = '91000000000'


# first_score, second_score
def common_process(first_score, second_score, therapist_phone, patient_phone , type_):
    therapist_send_mes(therapist_phone)
    therapist_token_value = therapist_login(therapist_phone)
    add_whitelist(therapist_token_value, patient_phone)
    patient_send_mes(patient_phone)
    token_value = patient_login(patient_phone)
    fill_in_information(token_value, '测试')
    # process接口获取到level_id
    level_id = get_process(token_value)
    # 获取到leveltoday的运动数据
    res_value = get_processtoday(token_value, level_id)
    levelSet_id = res_value['level']['levelSet']
    levelStats_id = res_value['level']['levelStats']
    # 填写昨日数据
    yesterday_data = {'sleepScore': second_score, 'backPainScore': first_score}
    yesterday_stats(token_value, yesterday_data)

    # 全局校准
    level_status_calibration(token_value, levelSet_id, levelStats_id, )

    for key in res_value['level']['exercises']:
        dict_data = {'levelSetId': levelSet_id, 'levelStatsId': levelStats_id, 'exerciseId': key['exercise']['_id'],
                     'repeat': key['repeat'], 'holdtime': key['holdtime'], 'skippedAt': -1, 'useSensor': True,
                     'sensorAngles': '123', 'mId': key['mId'],
                     'duration': 75935, 'battery': [50], 'preparationDuration': 30}  # 卡准备页时间
        # 循环请求提交单个次数
        level_stats(token_value, dict_data)
        dict_data_two = {
            'level': level_id,
            'levelStats': levelStats_id,
            'exercise': key['exercise']['_id'],
            'phase': type_,
            'battery': [50],
            'mId': key['mId'],
            # 'repeat': key['repeat'],
            'category': '进度条不动',
            'detail': 'test{}'.format(type_),
            'timestamps': 29554
        }
        error_report(token_value, dict_data_two)

    whole_one_data = {
        'fromPage': 'training',
        'levelStatsId': levelStats_id,
        'duration': 2469572,
        'score': 325
    }
    # 第一次请求whole，进入到患者评价
    stats_whole(token_value, whole_one_data)

    whole_two_data = {'levelStatsId': levelStats_id,
                      'feedback': 3,
                      'changeExercise': True}
    # 第二次请求whole，提交反馈
    stats_whole(token_value, whole_two_data)
    # cancel(token_value)
    return therapist_token_value, token_value


def test1():
    res = common_process(70, 50, '30000000000', '12343000000', 'setup')
    therapist_token_value = res[0]
    patient_token_value = res[1]
    res = search_warning(therapist_token_value, '12343000000')
    for i in res['patientAlerts']:
        print(i['errorPreparation'])


def test2():
    res = common_process(70, 50, '30000000000', '12345000000', 'movement')
    therapist_token_value = res[0]
    patient_token_value = res[1]
    res = search_warning(therapist_token_value, '12345000000')
    for i in res['patientAlerts']:
        print(i['errorMovement'])

